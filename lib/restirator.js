var merge = require('merge');
var format = require("string-template")

var defaultOptions = {
	version: 'v1',
	path: '/api/{version}/{model}/',
	idFormat: '{model}Id',
};

function restirator(db, app, options) {

	options = merge(defaultOptions, options);

	function decapitalize(s) {
		return s.charAt(0).toLowerCase() + s.slice(1)
	}

	function expressAPI(modelName, Model) {
		var m = options.model = decapitalize(modelName);
		var mId = format(options.idFormat, options);
		var p = format(options.path, options);

		console.log('expressAPI', kind, m, mId, p);

		// Merge possible model specific options
		var o = (typeof options[m] === "object") ? merge(options, options[m]) : options;

		app.param(mId, function(req, res, next, id) {
			Model.findById(req.params[mId], function(err, item) {
				// TODO: smart error handling
				if (err) res.status(404).end();
				else {
					req[m] = item;
					next();
				}
			});
		});

		// Find
		app.get(p, function(req, res, next) {
			Model.find(req.query, function(err, items) {
				// TODO: smart error handling
				console.log('items', items);
				res.json(items);
			});
		});

		// Create
		app.post(p, function(req, res, next) {
			var ob = merge(req.body || {}, req.query || {});
			// console.log('create', req.query, req.query());
			var item = new Model(ob);
			item.save(function(err, item) {
				if (err) {
					res.status(400).send({name:err.name, message:err.message});
				}
				else {
					res.json(item);
				}
			});
		});

		app.route(p + ':' + mId)
			// Read
			.get(function(req, res, next) {
				res.json(req[m]);
			})
			// Update
			.put(function(req, res, next) {
				var ob = merge(req.body || {}, req.query || {}, {$query:"asd"});
				req[m].set(ob);
				req[m].save(function(err, item) {
					// TODO: smart error handling
					res.json(item);
				});
			})
			// Delete
			.delete(function(req, res, next) {
				req[m].remove(function(err) {
					// TODO: smart error handling
					res.json({});
				});
			});
	}


	function restifyAPI(modelName, Model) {
		var m = options.model = decapitalize(modelName);
		var mId = format(options.idFormat, options);
		var p = format(options.path, options);

		console.log('restifyAPI', kind, m, mId, p);

		// Merge possible model specific options
		var o = (typeof options[m] === "object") ? merge(options, options[m]) : options;

		app.param(mId, function(req, res, next, id) {
			Model.findById(req.params[mId], function(err, item) {
				// TODO: smart error handling
				if (err) res.status(404).end();
				else {
					req[m] = item;
					next();
				}
			});
		});

		// Find
		app.get(p, function(req, res, next) {
			Model.find(req.query, function(err, items) {
				// TODO: smart error handling
				res.json(items);
			});
		});

		// Create
		app.post(p, function(req, res, next) {
			var ob = merge(req.body || {}, req.query || {});
			var item = new Model(ob);
			item.save(function(err, item) {
				if (err) {
					res.status(400).send({name:err.name, message:err.message});
				}
				else {
					res.json(item);
				}
			});
		});

		var p2 = p + ':' + mId;
		// Read
		app.get(p2, function(req, res, next) {
			res.json(req[m]);
		});
		// Update
		app.put(p2, function(req, res, next) {
			var ob = merge(req.body || {}, req.query || {});
			req[m].set(ob);
			req[m].save(function(err, item) {
				// TODO: smart error handling
				res.json(item);
			});
		});
		// Delete
		app.del(p2, function(req, res, next) {
			req[m].remove(function(err) {
				// TODO: smart error handling
				res.json({});
			});
		});
	}

	// Detect the framework
	var kind = app.engine ? "express" : "restify";

	var modelNames = db.modelNames();
	console.log(kind, 'modelNames', modelNames);
	for(var i = 0; i < modelNames.length; i++) {
		if (kind === "express") expressAPI(modelNames[i], db.model(modelNames[i]));
		else restifyAPI(modelNames[i], db.model(modelNames[i]));
	}
};

module.exports = restirator;

Restirator
==========

Restirator turns Mongoose Models automatically into an Express or Restify CRUD REST API.

Kickoff your Express / Restify server by just modeling the data with mongoose and turning that into a REST API that you can use from your frontend.

# Usage

First install the package (and required dependencies):

  	# npm install restirator body-parser mongoose --save


Then use it like this:

```JavaScript
var restirator = require('../lib/restirator');
var express = require('express');
var bodyParser = require('body-parser');

var app = express();

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));

var mongoose = require('mongoose/'),
	db = mongoose.connect('mongodb://localhost/restirator-test'),
	Schema = mongoose.Schema;

// Create a schema for our data
var MessageSchema = new Schema({
  message: String,
  number: Number,
  date: Date
});

// Use the schema to register a model with MongoDb
mongoose.model('Message', MessageSchema); 
var Message = mongoose.model('Message');

// Run our lib.
restirator(db, app);

// Serve the front-end assets (that consume the API).
app.use(express.static('public'));

module.exports = app;

if (!module.parent) {
  app.listen(port, host, function () {
    console.log("Express server listening on port %d in %s mode",
    app.address().port,
    app.settings.env
  );
});
```

# Generated API

- **Search**, GET `/api/v1/<model name>` with mongo search filters as query parameters
- **Create**, POST to `/api/v1/<model name>`
- **Read**, GET `/api/v1/<model name>/<object id>`
- **Update**, PUT `/api/v1/<model name>/<object id>`
- **Delete**, DELETE `/api/v1/<model name>/<object id>`


# Testing restirator

There is a problem in having Express and Restify in the same Node.js process, as they both overwite features in Node's native http-object to make things easier, but end up confusing each other. What this means is you need to run the Express and Restify tests separately. To do so:


    # mocha test/express_test.js 
    # mocha test/restify_test.js


# TODO

- [x] Configure path
- [x] Restify support
- [ ] CORS -headers
- [ ] Hapi support (if it makes sense)
- [ ] loopback support (if it makes sense)
- [ ] simple but solid authentication support (with mongoose plugins perhaps? or our own hooks? or register auth functions for express?)
- [ ] linking. [HATEOAS](https://spring.io/understanding/HATEOAS)?
- [ ] generate documentation and/or some kind of service descriptor
- [ ] "three kinds of wrong versioning" (configurable)
- [ ] http://jsonapi.org ?

# More material

Below are interesting articles that heavily influenced the original design of restirator:

- https://strongloop.com/strongblog/compare-express-restify-hapi-loopback/
- http://cdnjs.com/libraries/backbone.js/tutorials/nodejs-restify-mongodb-mongoose
- http://www.troyhunt.com/2014/02/your-api-versioning-is-wrong-which-is.html
- https://spring.io/understanding/HATEOAS
- http://51elliot.blogspot.fi/2013/08/testing-expressjs-rest-api-with-mocha.html

# License

The library is licensed under the MIT license, see [LICENSE](LICENSE) for more details.

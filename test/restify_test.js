var should = require('should');

describe('restify', function () {

  var app  = require('./restify_app.js');
  var port = 3004;

  var server;
  var url = 'http://localhost:' + port + '/api/v1/message';

  before (function (done) {
    server = app.listen(port, function (err, result) {
      if (err) {
        done(err);
      } else {
        done();
      }
    });
  });

  after(function (done) {
    server.close(done);
  });

  it('should exist', function (done) {
    should.exist(app);
    done();
  });

  require('./common_tests')(url);
});


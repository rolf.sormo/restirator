var request = require('request');
var should = require('should');

module.exports = function commonTests(url) {
  var m1, m2;

  it('should be listening at ' + url, function (done) {
    console.log('url', url);
    request.get({url:url}, function (err, res, body) {
      res.statusCode.should.eql(200);
      done(err);
    });
  });

  it('should be able to create new messages as post body', function (done) {
    var ob = {message:'Data in body', date: new Date(), number:5};
    request.post({url:url, json:true, body:ob},
        function(err, res, body) {
            res.statusCode.should.eql(200);
            m1 = body;
            done(err);
        }
    );
  });

  it('should be able to create new messages from query string', function (done) {
    var ob = {message:'Data in query string', date: new Date(), number:9};
    request.post({url:url, json:true, qs:ob},
        function(err, res, body) {
            res.statusCode.should.eql(200);
            m2 = body;
            done(err);
        }
    );
  });

  it('should be able to find messages by number', function (done) {
    request.get({url:url, qs:{number:9}},
        function(err, res, body) {
            res.statusCode.should.eql(200);
            done(err);
        }
    );
  });

  it('should be able to load messages by id', function (done) {
    request.get({url:url + '/' + m1._id, json:true},
        function(err, res, body) {
            res.statusCode.should.eql(200);
            body.number.should.eql(m1.number);

            request.get({url:url + '/' + m2._id, json:true},
                function(err, res, body) {
                  console.log(body);
                    res.statusCode.should.eql(200);
                    body.number.should.eql(m2.number);
                    done(err);
                }
            );
        }
    );
  });

  it('should be able to update messages as json', function (done) {
    var ob = {number:-12};
    request.put({url:url + '/' + m2._id, json:true, body:ob},
        function(err, res, body) {
            res.statusCode.should.eql(200);
            body.number.should.eql(ob.number);
            m2 = body;
            done(err);
        }
    );
  });

  it('should be able to update messages from query string', function (done) {
    var ob = {number:-97};
    request.put({url:url + '/' + m1._id, json:true, qs:ob},
        function(err, res, body) {
            res.statusCode.should.eql(200);
            body.number.should.eql(ob.number);
            m1 = body;
            done(err);
        }
    );
  });

  it('should be able to find messages with different searches', function (done) {
    request.get({url:url, json:true, qs:{number:m1.number}},
        function(err, res, body) {
            res.statusCode.should.eql(200);
            body.length.should.eql(1);
            body[0].should.eql(m1);
            request.get({url:url, json:true, qs:{number:{$gte:m1.number,$lte:m1.number}}},
                function(err, res, body) {
                    res.statusCode.should.eql(200);
                    body.length.should.eql(1);
                    body[0].should.eql(m1);
                    done(err);
                }
            );
        }
    );
  });


  it('should be able to remove the created messages', function (done) {
    request.del({url:url + '/' + m1._id, json:true},
        function(err, res, body) {
            res.statusCode.should.eql(200);
            request.del({url:url + '/' + m2._id, json:true},
                function(err, res, body) {
                    res.statusCode.should.eql(200);
                    done(err);
                }
            );
        }
    );
  });
};

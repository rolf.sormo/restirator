var mongoose = require('mongoose/'),
	db = mongoose.createConnection('mongodb://localhost/restirator-test'),
	Schema = mongoose.Schema;

// Create a schema for our data
var MessageSchema = new Schema({
  message: String,
  number: Number,
  date: Date
});

// Use the schema to register a model with MongoDb
mongoose.model('Message', MessageSchema); 
var Message = db.model('Message');

module.exports = db;
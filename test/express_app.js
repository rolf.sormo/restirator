var restirator = require('../lib/restirator');
var express = require('express');
var bodyParser = require('body-parser');

var app = express();

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));

var db = require('./db');

// Run our lib.
restirator(db, app);

module.exports = app;
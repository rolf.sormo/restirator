var restirator = require('../lib/restirator');
var restify = require('restify');
var bodyParser = require('body-parser');

var app = restify.createServer()

app.use(restify.queryParser());
app.use(restify.bodyParser());

var db = require('./db');

// Run our lib.
restirator(db, app);

module.exports = app;